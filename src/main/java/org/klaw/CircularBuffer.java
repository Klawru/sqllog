package org.klaw;

public class CircularBuffer<E> {

    private final E[] array;
    private final int maxSize;
    private int start, end;
    private boolean full = false;

    public CircularBuffer(final int size) {
        array = (E[]) new Object[size];
        maxSize = array.length;
    }

    public E add(final E element) {
        E removed = null;
        if (isFull()) {
            removed = remove();
        }
        array[end++] = element;
        if (end == maxSize)
            end = 0;
        if (end == start)
            full = true;
        return removed;
    }

    public E remove() {
        final E element = array[start];
        if (null != element) {
            array[start++] = null;
            if (start == maxSize)
                start = 0;
            full = false;
        }
        return element;
    }

    private boolean isFull() {
        return size() == maxSize;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        int size = 0;
        if (end < start)
            size = maxSize - start + end;
        else if (end == start)
            size = full ? maxSize : 0;
        else
            size = end - start;
        return size;
    }

}
