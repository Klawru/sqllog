package org.klaw;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogParser {
    private LinkedHashMap<String, StringBuilder> hashMap = new LinkedHashMap<String, StringBuilder>();
    private Matcher varMatcher = Pattern.compile("[(, \t]:([\\d]+)").matcher("");
    private Matcher bindMatcher = Pattern.compile("(?:^|e |\\t)(\\d+): (.*)$").matcher("");
    private CircularBuffer<StringBuilder> lineBuffer = new CircularBuffer<StringBuilder>(1000);

    public void parse(MyBufferedReader reader, BufferedWriter writer) throws IOException {
        try {
            StringBuilder line = reader.readLineToBuilder(true);
            while (line != null) {
                //Сохранение в буфер, и при переполнении запись в Writer
                putBuffer(writer, line);
                //Игнорируем пустые строки
                if (line.length() > 0) {
                    findPatternInLine(line);
                }
                //Считываем следущую строку
                line = reader.readLineToBuilder(true);
            }
            //Запись оставшихся строк из буфера
            StringBuilder item = lineBuffer.remove();
            while (item != null) {
                writer.append(item);
                writer.newLine();
                item = lineBuffer.remove();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            reader.close();
            writer.close();
        }
    }

    private void findPatternInLine(StringBuilder line) {
        //Ищем переменные в строке
        varMatcher.reset(line);
        boolean isFind = varMatcher.find();
        if (isFind) {
            hashMap.put(varMatcher.group(1), line);
            while (varMatcher.find()) {
                hashMap.put(varMatcher.group(1), line);
            }
        } else {
            //Ищем значения для подстановки, и заменяем если нашли
            bindMatcher.reset(line);
            while (bindMatcher.find()) {
                String group = bindMatcher.group(1);
                StringBuilder first = hashMap.remove(group);
                if (first != null) {
                    int startInd = first.indexOf(":" + group);
                    if (startInd > 0)
                        first.replace(startInd, startInd + group.length() + 1, "'" + bindMatcher.group(2) + "'");
                }
            }
        }
    }

    private void putBuffer(BufferedWriter writer, StringBuilder line) throws IOException {
        StringBuilder bufferedLine = lineBuffer.add(line);
        if (bufferedLine != null) {
            writer.append(bufferedLine);
            writer.newLine();
        }
    }
}
